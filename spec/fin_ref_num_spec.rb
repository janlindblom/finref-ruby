RSpec.describe FinRefNum do
  it 'has a version number' do
    expect(FinRefNum::VERSION).not_to be nil
  end

  it 'generates expected control numbers' do
    basis = 123456
    code = FinRefNum.calculate_verification_code basis
    expect(code).to eq 1
    code = FinRefNum.calculate_verification_code basis.to_s
    expect(code).to eq 1
  end

  it 'can generate an Integer reference number' do
    basis = 10000 + Random.rand(99999)
    refnum = FinRefNum.generate_one(basis)
    expect(refnum).to be_a Integer
    expect(FinRefNum.valid? refnum).to be true
  end

  it 'can generate a String reference number' do
    basis = 10000 + Random.rand(99999)
    refnum = FinRefNum.generate_one(basis.to_s)
    expect(refnum).to be_a String
    expect(FinRefNum.valid? refnum).to be true
  end

  it 'can validate a reference number' do
    refnum = 1234561
    expect(FinRefNum.valid? refnum).to be true
    expect(FinRefNum.valid? refnum.to_s).to be true
  end
end
