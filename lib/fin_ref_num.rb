require 'fin_ref_num/version'

# Calculate invoice reference numbers for invoices in Finland.
#
# @author Jan Lindblom <jan@robotika.ax>
module FinRefNum

  # Calculate a verification code for a given basis.
  #
  # @example
  #   FinRefNum.calculate_verification_code 123456
  #   #=> 1
  #
  # @param [String, Integer] basis the basis to calculate a verification
  #   code for.
  # @return [Integer] the calculated verification code.
  def self.calculate_verification_code(basis)
    basis = basis.to_s unless basis.is_a? String
    sum = 0
    basis.reverse.chars.map(&:to_i).each_with_index do |num, i|
      sum += num * [7, 3, 1][i % 3]
    end
    (10 - (sum % 10)) % 10
  end

  # Generate one reference number based on a given basis.
  #
  # @example Generate an Integer
  #   FinRefNum.generate_one 123456
  #   #=> 1234561
  # @example Generate a String
  #   FinRefNum.generate_one "123456"
  #   #=> "1234561"
  #
  # @param [String, Integer] basis the basis to form the number from.
  # @return [String, Integer] a reference number for a given basis.
  def self.generate_one(basis)
    input_is_string = basis.is_a? String
    code = calculate_verification_code(basis)
    reference = "#{basis}#{code}"
    return reference if input_is_string

    reference.to_i
  end

  # Generate many reference numbers based on a given basis.
  #
  # @example Generate Integers
  #   FinRefNum.generate_many 123456, 5
  #   #=> [1234574, 1234587, 1234590, 1234600, 1234613]
  # @example Generate Strings
  #   FinRefNum.generate_one "123456", 5
  #   #=> ["1234574", "1234587", "1234590", "1234600", "1234613"]
  #
  # @param [String, Integer] basis the basis to form the numbers from.
  # @return [Array<String>, Array<Integer>] an Array with reference number for
  #   a given basis.
  def self.generate_many(basis, amount)
    input_is_string = basis.is_a? String
    numbers = []
    (1..amount).each do |num|
      reference = "#{basis.to_i + num}"
      reference += calculate_verification_code(reference).to_s
      reference = reference.to_i unless input_is_string
      numbers << reference
    end
    numbers
  end

  # Check if a reference number is valid
  #
  # @param [Integer, String] number the reference number to validate.
  # @return [Boolean] +true+ or +false+.
  def self.valid?(number)
    control = number.to_s.chars.last.to_i
    basis = number.to_s[0..-2]
    calculated = calculate_verification_code basis
    calculated == control
  end
end
